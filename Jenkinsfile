// Ultimo pipeline configurado con etapas de testing para OWASP ZAP 21/12/2023

pipeline {
    agent any

    environment {
        TEMPLATE_PATH = "@/usr/local/share/trivy/templates/html.tpl"
        DOCKERHUB_CREDENCIALS = credentials('credentials-dockerhub')
        BUILD_NUMBER = "${BUILD_NUMBER}"
        RepoDockerHub = 'ngissara'
        NameContainer = 'devops'
        ARGOSERVER = '192.168.0.59:8090'
		REPO_URL = 'https://gitlab.com/nicolas.gissara/flask-pokedex.git'
		APP_PORT = '5000'
        ZAP_CONTAINER_NAME = 'owasp-zap'
        ARGONAMESPACE = 'argocd'
        //ARGOCD_USERNAME = credentials('argo-username-credentials') // Credenciales para el nombre de usuario
        //ARGOCD_PASSWORD = credentials('argo-password-credentials') // Credenciales para la contraseña
        //ARGOCD_TOKEN = credentials('argo-token-credentials2') // Asegúrate de crear credenciales secretas en Jenkins para el token
    }
	
    stages {
        				
        stage('Build and deployd Test OWASP ZAP') {
            steps {
                script {
                    def appContainerName = 'PokeAcu'
                    /*
                    echo "Eliminando el directorio existente..."
                    sh "rm -rf flask-pokedex"
                    
                    echo "Clonando el repositorio desde GitLab..."
                    sh "git clone ${REPO_URL}"
                    */
                    echo "Construyendo la imagen Docker..."
                    sh "docker build -t ${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER} ."

                    // Detener y eliminar el contenedor de la aplicación si está en ejecución
                    sh "docker stop ${appContainerName} || true"
                    sh "docker rm ${appContainerName} || true"

                    // Iniciar el contenedor de la aplicación
                    sh "docker run -d --name ${appContainerName} -p ${APP_PORT}:5000 ${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER}"
                }
            }
        }
        stage('Ejecutar ZAP-baseline con Docker') {
            steps {
                script {
                   

                   // Iniciar el contenedor ZAP si no existe
                    sh 'docker run --name zap-container -u zap -v $(pwd)/zap-wrk:/zap/wrk -p 8090:8080 -d owasp/zap2docker-stable sleep infinity'
                   

                   // Verificar si el directorio /zap/wrk existe
                    //def directoryExists = sh(script: 'docker exec zap-container test -d /zap/wrk', returnStatus: true)

                    // Si el directorio no existe, créalo
                    //if (directoryExists != 0) {
                    //    echo 'Creating /zap/wrk directory'
                     //   sh 'docker exec zap-container mkdir /zap/wrk'
                    //}
                   
                    // Ejecutar ZAP-baseline dentro de un contenedor Docker
                    //sh 'docker run --name zap-container -u zap -p 9090:8080 -d owasp/zap2docker-stable zap-baseline.py -t http://192.168.0.33:5000 -g gen.conf -r zap_report.html'
                    //sh 'docker run --name zap-container -u zap -v $(pwd)/zap-wrk:/zap/wrk -p 8090:8080 -d owasp/zap2docker-stable zap-baseline.py -t http://192.168.0.33:5000 -g gen.conf -r zap_report.html'
                    //sh 'docker run --name zap-container -v $(pwd):/zap/wrk/:rw -t owasp/zap2docker-stable zap-api-scan.py -t http://192.168.0.33:5000 -f openapi -d -r zap_report.html'
                    //sh 'docker exec zap-container zap-baseline.py -t http://192.168.0.33:5000 -g gen.conf -r /zap/wrk/zap_report.html'
                    sh 'docker exec zap-container zap-baseline.py -t http://192.168.0.33:5000 -g gen.conf -r /zap/wrk/zap_report.html || true'

                    

                    // Esperar a que el escaneo termine (ajustar según sea necesario)
                    //sleep time: 1, unit: 'MINUTES'

                    // Copiar el informe fuera del contenedor Docker
                    sh 'docker cp zap-container:/zap/wrk/zap_report.html .'
                    
                    // Detener y eliminar el contenedor Docker
                    //sh 'docker stop zap-container'
                    //sh 'docker rm zap-container'
                }
            }
        }

        stage('Publicar resultados') {
            steps {
                script {
                    // Publicar resultados (ajustar según sea necesario)
                    archiveArtifacts artifacts: 'zap_report.html', fingerprint: true
                }
            }
        }
                 
        /*stage('Remove Test Build ') {
            steps {
                script {
                    def imageToDelete = "${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER}"
                    echo "Deteniendo y eliminando el contenedor OWASP ZAP..."
                    sh "docker stop ${ZAP_CONTAINER_NAME}"
                    sh "docker rm ${ZAP_CONTAINER_NAME}"
                    //sh "docker rmi ${imageToDelete}"
                }
            }
        }*/

        stage('OWASP Dependency Scan') {
            steps {
                dependencyCheck additionalArguments: '''
                    -o "./"
                    -s "./"
                    -f "ALL"
                    --prettyPrint ''', odcInstallation: 'DP-Check'
                dependencyCheckPublisher pattern: '**/dependency-check-report.xml'
            }
        }
		
		stage('Analyze with Trivy') {
            steps {
                echo "Analyze with Trivy"
                sh "trivy image --format template --template ${TEMPLATE_PATH} --severity HIGH,CRITICAL -o cve_report.html ${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER}"
            }
        }
		
		stage('Login to Dockerhub') {
            steps {
                sh "echo $DOCKERHUB_CREDENCIALS_PSW | docker login -u $DOCKERHUB_CREDENCIALS_USR --password-stdin "
            }
        }
        
		stage('Push image to Dockerhub') {
            steps {
                sh "docker build -t ${env.RepoDockerHub}/${env.NameContainer}:latest ."
                sh "docker push ${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER}"
                sh "docker push ${env.RepoDockerHub}/${env.NameContainer}:latest"
            }
        }
    }

    post {
        always {
            script {
                archiveArtifacts artifacts: "cve_report.html", fingerprint: true
                publishHTML (target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: '.',
                    reportFiles: 'cve_report.html',
                    reportName: "CVE Report"
                ])
            }
        }
        
        success {
            script {
                // Publicar el informe HTML para visualización y descarga
                archiveArtifacts artifacts: "zap_report.html", fingerprint: true
                publishHTML (target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: '.',
                    reportFiles: 'zap_report.html',
                    reportName: "OWASP ZAP Report"
                ])
            }
        }
    }
}